package com.mediio.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MediioApplication {

    public static void main(String[] args) {
        SpringApplication.run(MediioApplication.class, args);
    }

}
